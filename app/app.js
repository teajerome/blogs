var express = require('express')
var mongoose = require('mongoose')
var app = express()
var bodyParser = require('body-parser')
var passport = require('passport')
var config = require('./config/main')
var errorHandler = require('./middleware/errorHandler')


var port = 3000

app.use(express.static(__dirname + "/../public"))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(passport.initialize())

mongoose.Promise = global.Promise
mongoose.connect(config.database)


require('./config/passport')(passport)

require(__dirname + '/routes')(app)

app.use(errorHandler)

app.listen(port)

console.log('server running on port ' + port)