var blog = require('./blogController')

module.exports = [
    // POST
    {
        path: '/blogs',
        httpMethod: 'POST',
        middleware: [blog.insert]
    },
    // DELETE
    {
        path: '/blogs/:id',
        httpMethod: 'DELETE',
        middleware: [blog.delete]
    },
    // PUT
    {
        path: '/blogs/:id',
        httpMethod: 'PUT',
        middleware: [blog.update]
    },
    // GET
    {
        path: '/blogs/:text',
        httpMethod: 'GET',
        middleware: [blog.searchContent]
    },
    {
        path: '/blogs/:id/comments',
        httpMethod: 'GET',
        middleware: [blog.getBlogById]
    },
    {
        path: '/blogs/:currentPage/:itemsPerPage',
        httpMethod: 'GET',
        middleware: [blog.getList]
    }
]