var mongoose = require('mongoose')
var User = require('../user/userModel')
var Comment = require('../comment/commentModel')
var Schema = mongoose.Schema

var BlogSchema = new Schema({
    author: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true,
    },
    text: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    comments: [{
        type: Schema.ObjectId,
        ref: 'Comment',
        required: false
    }],
    sumOfRatingPoint: {
        type: Number
    },
    numberOfRatings: {
        type: Number
    },
    updatedAt: {
        type: Date
    }
})

module.exports = mongoose.model('Blog', BlogSchema)