var Blog = require('./blogModel')

module.exports = {

    // Save A New Blog
    insert: function (req, res, next) {
        var blog = new Blog({
            author: req.user._id,
            text: req.body.text
        })
        blog.save(function (err, success) {
            if (err) next({ status: 400, message: err.message })
            else res.status(200).json(blog)
        })
    },

    // Delete Blog
    delete: function (req, res, next) {
        var id = req.params.id
        Blog.remove({ _id: id }).exec(function (err, success) {
            if (err) next({ status: 400, message: err.message })
            else res.status(200).json('Blog : ' + id + ' has been deleted')
        })
    },

    // Update
    update: function (req, res, next) {
        var id = req.params.id
        var text = req.body.text
        var updatedAt = new Date()
        Blog.findByIdAndUpdate({ _id: id }, { $set: { text, updatedAt } }, function (err, success) {
            console.log(success)
            if (err) next({ status: 400, message: err.message })
            else res.status(200).json(success)
        })
    },

    // Fetch Blogs By Page
    getList: function (req, res, next) {
        var currentPage = req.params.currentPage
        var itemsPerPage = Number(req.params.itemsPerPage)
        Blog.count({})
            .then(count => {
                Blog.find({})
                    .limit(itemsPerPage)
                    .populate('author')
                    .skip(itemsPerPage * currentPage)
                    .sort({ createdAt: -1 })
                    .exec(function (err, blogs) {
                        if (err) next({ status: 400, message: err.message })
                        else {
                            var numberOfPages = Math.ceil(count / itemsPerPage)
                            res.status(200).json({ blogs, numberOfPages })
                        }
                    })
            })
            .catch(err => next({ status: 400, message: err.message }))
    },

    // Search Content
    searchContent: function (req, res, next) {
        var contentToSearch = req.params.text
        Blog.find({ 'text': new RegExp(contentToSearch, "i") }, function (err, doc) {
            if (err) next({ status: 400, message: err.message })
            else res.status(200).json(doc)
        });
    },

    /* 
    Get One Blog
    Populate With The Author
    Populate With The Comments 
    Populate Comments With The Reviewers Username */
    getBlogById: function (req, res) {
        Blog.findOne({ _id: req.params.id })
            .populate('author')
            .populate({ path: 'comments', populate: { path: 'author' } })
            .exec(function (err, success) {
                if (err) next({ status: 400, message: err.message })
                else res.status(200).json(success)
            })
    }

}