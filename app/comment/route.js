var comment = require('./commentController')

module.exports = [
    // POST
    {
        path: '/comments',
        httpMethod: 'POST',
        middleware: [comment.add]
    }
]