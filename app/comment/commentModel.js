var mongoose = require('mongoose')
var User = require('../user/userModel')
var Blog = require('../blog/blogModel')
var Schema = mongoose.Schema

var CommentSchema =  new Schema({
    blog : {
        type: Schema.ObjectId,
        ref:'Blog',
        required: true
    },
    author : { 
        type:  Schema.ObjectId,
        ref:'User',
        required: true
    },
    text : {
        type: String,
        require: true,
    },
    createdAt:{
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('Comment', CommentSchema)