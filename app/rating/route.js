var rating = require('./ratingController')

module.exports = [
    // POST
    {
        path: '/ratings',
        httpMethod: 'POST',
        middleware: [rating.add]
    },
    // GET
    {
        path: '/ratings/didrate/:blogId',
        httpMethod: 'GET',
        middleware: [rating.didRate]
    }
]