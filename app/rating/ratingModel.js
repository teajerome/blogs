var mongoose = require('mongoose')
var User = require('../user/userModel')
var Blog = require('../blog/blogModel')
var Schema = mongoose.Schema

var RatingSchema = new Schema({
    user: { type: Schema.ObjectId, ref: 'User', required: true },
    blog: { type: Schema.ObjectId, ref: 'Blog' },
    value: { type: Number, default: 1, required: true }
})

module.exports = mongoose.model('Rating', RatingSchema)