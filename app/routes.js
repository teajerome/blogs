var BlogRoute = require('./blog/route')
var CommentRoute = require('./comment/route')
var RatingRoute = require('./rating/route')
var UserRoute = require('./user/route')
var isLoggedIn = require('./middleware/auth')
var _ = require('underscore')

var routes = _.flatten([BlogRoute, CommentRoute, RatingRoute, UserRoute])


module.exports = function (app) {

    _.each(routes, route => {

        if (
            !(  // Don't Add Auth Middleware To Register And Log Routes
                (route.path == '/users' && route.httpMethod == 'POST') ||
                (route.path == '/users/authentication' && route.httpMethod == 'POST'))
        ) {
            route.middleware.unshift(isLoggedIn)
        }

        var args = _.flatten([route.path, route.middleware])

        switch (route.httpMethod.toUpperCase()) {
            case 'POST':
                app.post.apply(app, args)
                break
            case 'DELETE':
                app.delete.apply(app, args)
                break
            case 'PUT':
                app.put.apply(app, args)
                break
            case 'GET':
                app.get.apply(app, args)
                break
            default:
                throw new Error('Invalid HTTP method specified for route ' + route.path);
                break
        }
    })
    // Default
    app.get('*', (req, res) => {
        res.redirect('/');
    })


}