var JwtStrategy = require('passport-jwt').Strategy
var ExtractJwt = require('passport-jwt').ExtractJwt
var User = require('../user/userModel')
var config = require('./main')


module.exports = function (passport) {
    let opts = {}
    opts.jwtFromRequest = ExtractJwt.fromAuthHeader()
    opts.secretOrKey = config.secret
    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
        User.findOne({ username: jwt_payload.username })
            .then(user => done(null, user))
            .catch(err => done(err, false))
    }))
}