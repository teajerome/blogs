var user = require('./userController')

module.exports = [
    // POST
    {
        path: '/users',
        httpMethod: 'POST',
        middleware: [user.register]
    },
    {
        path: '/users/authentication',
        httpMethod: 'POST',
        middleware: [user.authenticate]
    },
    // GET
    {
        path: '/users',
        httpMethod: 'GET',
        middleware: [user.getUsername]
    }

]