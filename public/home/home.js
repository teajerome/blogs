myApp.controller('homeController', function ($scope, $http, $window, $location, $rootScope) {

    if (localStorage.token)
        $location.path('/blogs')

    // Register
    $scope.register = function () {
        if (!$scope.newUsername || !$scope.newPassword || !$scope.newRetypePassword) {
            return $window.alert('Veuillez remplir tous les champs')
        }
        if ($scope.newPassword !== $scope.newRetypePassword) {
            return $window.alert('Les mots de passe ne sont pas identiques')
        }
        $http.post('/users', {
            username: $scope.newUsername,
            password: $scope.newPassword
        })
    }

    // Authenticate
    $scope.authenticate = function () {
        if (!$scope.username || !$scope.username) {
            return $window.alert('Veuillez remplir tous les champs')
        }
        $http.post('/users/authentication', {
            username: $scope.username,
            password: $scope.password
        }).success(res => {
            $window.localStorage.token = res.token
            $location.path('/blogs')
            $rootScope.$emit('onConnect')
        })
    }

})