myApp.factory('pagination', function () {
    return {

        maxPagination: 10,
        generatePageList: function (currentPage, currentListPage, lastPage, cb) {
            var newListPage = []
            if (currentPage <= 5 || ((currentPage > lastPage - 4) && currentListPage.includes(lastPage)))
                cb(currentListPage)
            else if (currentPage + 5 > lastPage) {
                for (var i = lastPage - 10; i <= lastPage; i++) {
                    newListPage.push(i)
                }
                cb(newListPage)
            }
            else {
                for (var i = currentPage - 5; i < currentPage + 5; i++) {
                    newListPage.push(i)
                }
                cb(newListPage)
            }
        },

        generateFirstPagination: function (totalOfPages, cb) {
            var firstPagination = []
            if (totalOfPages >= this.maxPagination) {
                for (var i = 0; i <= maxPagination; i++) {
                    firstPagination.push(i)
                }
            }
            else {
                for (var i = 1; i <= totalOfPages; i++) {
                    firstPagination.push(i)
                }
            }
            cb(firstPagination)
        }
    }
})
